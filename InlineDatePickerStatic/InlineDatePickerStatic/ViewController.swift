//
//  ViewController.swift
//  InlineDatePickerStatic
//
//  Created by Patrick Dawson on 05.01.16.
//  Copyright © 2016 Patrick Dawson. All rights reserved.
//
// Important note: All time cells that have an inline datepicker below them should have the tag 100.

import UIKit

class ViewController: UITableViewController {
    // MARK: - Constants
    let kDateCellTag = 100
    let kStartTimePickerIndexPath = NSIndexPath(forRow: 1, inSection: 0)
    let kStopTimePickerIndexPath = NSIndexPath(forRow: 3, inSection: 0)
    
    // MARK: - Outlets
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var stopTimeLabel: UILabel!
    @IBOutlet weak var startTimePickerCell: UITableViewCell!
    @IBOutlet weak var startTimePicker: UIDatePicker!
    @IBOutlet weak var stopTimePicker: UIDatePicker!
    
    // MARK: - Properties
    var dateFormatter: NSDateFormatter!
    var pickerCellRowHeight: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        dateFormatter = NSDateFormatter()
        dateFormatter.locale = NSLocale.currentLocale()
        dateFormatter.timeStyle = .ShortStyle
        let now = NSDate()
        startTimeLabel.text = dateFormatter.stringFromDate(now)
        stopTimeLabel.text = dateFormatter.stringFromDate(now)
        
        // Store orignal height
        pickerCellRowHeight = startTimePickerCell.frame.height
        startTimePicker.hidden = true
        stopTimePicker.hidden = true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITableViewDataSource
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return super.tableView(tableView, numberOfRowsInSection: section)
    }
    
    // MARK: - UITableViewDelegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let selectedCell = tableView.cellForRowAtIndexPath(indexPath) {
            if selectedCell.tag == kDateCellTag {
                // todo toggle picker
                toggleDatePickerForSelectedIndexPath(indexPath)
            } else {
                tableView.deselectRowAtIndexPath(indexPath, animated: true)
            }
        }
        
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        var height = tableView.rowHeight
        
        if indexPath == kStartTimePickerIndexPath {
            height = startTimePicker.hidden ? CGFloat(0) : pickerCellRowHeight
        } else if indexPath == kStopTimePickerIndexPath {
            height = stopTimePicker.hidden ? CGFloat(0) : pickerCellRowHeight
        }
        
        return height
    }
    
    // MARK: - Custom
    func hasInlineDatePicker() -> Bool {
        return !startTimePicker.hidden || !stopTimePicker.hidden
    }
    
    func toggleDatePickerForSelectedIndexPath(indexPath: NSIndexPath) {
        let targetedIndexPath = NSIndexPath(forRow: indexPath.row + 1, inSection: indexPath.section)
        
        if targetedIndexPath == kStartTimePickerIndexPath {
            startTimePicker.hidden = !startTimePicker.hidden
            stopTimePicker.hidden = true
        } else if targetedIndexPath == kStopTimePickerIndexPath {
            startTimePicker.hidden = true
            stopTimePicker.hidden = !stopTimePicker.hidden
        }
        
        // This causes table animation
        tableView.beginUpdates()
        tableView.endUpdates()
    
    }
    
    // MARK: - Actions
    @IBAction func timeChanged(sender: UIDatePicker) {
        let time = dateFormatter.stringFromDate(sender.date)
        
        if sender == startTimePicker {
            startTimeLabel.text = time
        } else {
            stopTimeLabel.text = time
        }
    }
}

