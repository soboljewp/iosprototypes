//
//  ViewController.swift
//  InlineDatePicker
//
//  Created by Patrick Dawson on 04.01.16.
//  Copyright © 2016 Patrick Dawson. All rights reserved.
//

import UIKit

class ViewController: UITableViewController {

    // MARK: - Constants
    let kDatePickerTag = 99
    let kTitleKey = "title"
    let kDateKey = "date"
    
    // keep track of which rows have date cells
    let kDateStartRow = 1
    let kDateEndRow = 2
    
    let kDateCellID = "dateCell"
    let kDatePickerID = "datePicker"
    let kOtherCellID = "otherCell"
    
    // MARK: - Properties
    var dataArray = [AnyObject]()
    var datePickerIndexPath: NSIndexPath?
    var dateFormatter = NSDateFormatter()
    var pickerCellRowHeight: CGFloat = 0
    
    // MARK: - View
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let item1 = [kTitleKey: "Tap a cell to change its date:"]
        let item2 = [kTitleKey: "Start Date:", kDateKey: NSDate()]
        let item3 = [kTitleKey: "End Date:", kDateKey: NSDate()]
        let item4 = [kTitleKey: "Other items1"]
        let item5 = [kTitleKey: "Other items1"]
        dataArray = [item1, item2, item3, item4, item5]
        
        // configure dateFormatter to show only time.
        dateFormatter.dateFormat = "hh:mm"
        
        // obtain the picker view cell's height, works because the cell was pre-defined in our storyboard
        let pickerViewCellToCheck = tableView .dequeueReusableCellWithIdentifier(kDatePickerID)
        pickerCellRowHeight = pickerViewCellToCheck!.frame.height
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem()
        /*
        let today = NSDate()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "hh:mm"
        
        startTimeLabel.detailTextLabel!.text = dateFormatter.stringFromDate(today)
        stopTimeLabel.detailTextLabel!.text = dateFormatter.stringFromDate(today)
        durationLabel.detailTextLabel!.text = "00:00"*/
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Custom
    func hasPickerForIndexPath(indexPath: NSIndexPath) -> Bool {
        var hasDatePicker = false
        
        var targetedRow = indexPath.row
        ++targetedRow
        
        let checkDatePickerCell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: targetedRow, inSection: 0))
        if let _ = checkDatePickerCell?.viewWithTag(kDatePickerTag) {
            hasDatePicker = true
        }
        return hasDatePicker
    }
    
    func updateDatePicker() {
        if let dpIndexPath = datePickerIndexPath {
            let associatedDatePickerCell = tableView.cellForRowAtIndexPath(dpIndexPath)
            if let targetedDatePicker = associatedDatePickerCell?.viewWithTag(kDatePickerTag) as? UIDatePicker{
                let itemData = dataArray[dpIndexPath.row-1]
                targetedDatePicker.setDate(itemData[kDateKey] as! NSDate, animated: false)
            }
        }
    }
    
    func hasInlineDatePicker() -> Bool {
        return datePickerIndexPath != nil
    }
    
    func indexPathHasPicker(indexPath: NSIndexPath) -> Bool {
        return hasInlineDatePicker() && datePickerIndexPath?.row == indexPath.row
    }
    
    func indexPathHasDate(indexPath: NSIndexPath) -> Bool {
        var hasDate = false
        
        if ((indexPath.row == kDateStartRow) || (indexPath.row == kDateEndRow || (hasInlineDatePicker() && (indexPath.row == kDateEndRow + 1)))) {
            hasDate = true
        }
        
        return hasDate
    }
    
    func toggleDatePickerForSelectedIndexPath(indexPath: NSIndexPath) {
        tableView.beginUpdates()
        
        let indexPaths = [NSIndexPath(forRow: indexPath.row + 1, inSection: 0)]
        
        // check if 'indexPath' has an attached date picker below it
        if hasPickerForIndexPath(indexPath) {
            // found a picker below it, so remove it
            tableView.deleteRowsAtIndexPaths(indexPaths, withRowAnimation: .Fade)
        } else {
            // didn't find a picker below it, so we should insert it
            tableView.insertRowsAtIndexPaths(indexPaths, withRowAnimation: .Fade)
        }
        
        tableView.endUpdates()
    }
    
    func displayInlineDatePickerForRowAtIndexPath(indexPath: NSIndexPath) {
        tableView.beginUpdates()
        
        var before = false
        if hasInlineDatePicker() {
            before = datePickerIndexPath!.row < indexPath.row
        }
        
        var sameCellClicked = false
        if let datePickerRow = datePickerIndexPath?.row {
            sameCellClicked = datePickerRow-1 == indexPath.row
        }
        
        // remove any date picker cell if it exists
        if hasInlineDatePicker() {
            tableView.deleteRowsAtIndexPaths([NSIndexPath(forRow: datePickerIndexPath!.row, inSection: 0)], withRowAnimation: .Fade)
            datePickerIndexPath = nil
        }
        
        if (!sameCellClicked) {
            // hide the old date picker and display the new one
            let rowToReveal = before ? indexPath.row - 1 : indexPath.row
            let indexPathToReveal = NSIndexPath(forRow: rowToReveal, inSection: 0)
            
            toggleDatePickerForSelectedIndexPath(indexPathToReveal)
            datePickerIndexPath = NSIndexPath(forRow: indexPathToReveal.row + 1, inSection: 0)
        }
        
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        tableView.endUpdates()
        updateDatePicker()
    }
    
    
    
    // MARK: - UITableViewDataSource
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return indexPathHasPicker(indexPath) ? pickerCellRowHeight : tableView.rowHeight
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if hasInlineDatePicker() {
            var numRows = dataArray.count
            return ++numRows
        }
        return dataArray.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        var cellID = kOtherCellID
        
        if (indexPathHasPicker(indexPath)) {
            cellID = kDatePickerID
        } else if (indexPathHasDate(indexPath)) {
            cellID = kDateCellID
        }
        
        cell = tableView.dequeueReusableCellWithIdentifier(cellID)
        
        if (indexPath.row == 0) {
            // we decide here that first cell in the table is not selectable (it's just an indicator)
            cell.selectionStyle = .None
        }
        
        // if we have a date picker open whose cell is above the cell we want to update,
        // then we have one more cell than the model allows
        //
        var modelRow = indexPath.row
        if let dpIndexPath = datePickerIndexPath {
            if dpIndexPath.row <= indexPath.row {
                --modelRow
            }
        }
        
        let itemData = dataArray[modelRow]
        
        // proceed to configure our cell
        if cellID == kDateCellID {
            // we have either start or end date cells, populate their date field
            //
            cell.textLabel!.text = itemData[kTitleKey] as? String
            cell.detailTextLabel!.text = dateFormatter.stringFromDate(itemData[kDateKey] as! NSDate)
        } else if cellID == kOtherCellID {
            cell.textLabel!.text = itemData[kTitleKey] as? String
        }
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let cell = tableView.cellForRowAtIndexPath(indexPath) {
            if cell.reuseIdentifier == kDateCellID {
                displayInlineDatePickerForRowAtIndexPath(indexPath)
            } else {
                tableView.deselectRowAtIndexPath(indexPath, animated: true)
            }
        }
        
    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    @IBAction func dateAction(sender: UIDatePicker) {
        var targetedCellIndexPath: NSIndexPath
        
        if hasInlineDatePicker() {
            targetedCellIndexPath = NSIndexPath(forRow: datePickerIndexPath!.row - 1, inSection: 0)
        } else {
            targetedCellIndexPath = tableView.indexPathForSelectedRow!
        }
        
        let cell = tableView.cellForRowAtIndexPath(targetedCellIndexPath)
        let targetedDatePicker = sender
        
        // update our data model
        var itemData = dataArray[targetedCellIndexPath.row] as! [String:NSObject]
        itemData[kDateKey] = targetedDatePicker.date
        dataArray[targetedCellIndexPath.row] = itemData
        
        cell?.detailTextLabel!.text = dateFormatter.stringFromDate(targetedDatePicker.date)
    }

}
